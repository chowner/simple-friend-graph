#!/usr/bin/env python3

import csv
import random

from graph_tool.all import graph_draw, Graph


def rand_color():
    """
    Returns a random color in hex format
    """
    return "#{:06x}".format(random.randint(0, 0xFFFFFF))


def add_to_people_vertices(graph, person, people, vertex_names,
                           vertex_colors):
    """
    Takes a graph, a map of vertex names, a map of vertex
    colors, a person's name (string) and a people
    vertex map name (key), vertex (value) and adds a new
    vertex to the graph if it doesn't exist already.
    Returns the vertex for the person.
    """
    if person not in people:
        vert = graph.add_vertex()
        vertex_names[vert] = person
        vertex_colors[vert] = rand_color()
        people[person] = vert
    else:
        vert = people[person]
    return vert

def generate_graph(connection_file):

    """
    Generates a friend network graph given a csv in the following
    format:
    Person1,Person2,Connection_Strength
    Outputs the graph to a png called friend_network_graph.png
    """
    graph = Graph(directed=False)

    v_name = graph.new_vertex_property("string")
    v_color = graph.new_vertex_property("string")
    e_weight = graph.new_edge_property("int")

    people_vertices = {}
    with open(connection_file, 'r') as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            vert0 = add_to_people_vertices(graph=graph,
                                           person=row[0],
                                           people=people_vertices,
                                           vertex_names=v_name,
                                           vertex_colors=v_color)
            vert1 = add_to_people_vertices(graph=graph,
                                           person=row[1],
                                           people=people_vertices,
                                           vertex_names=v_name,
                                           vertex_colors=v_color)
            e0_1 = graph.add_edge(vert0, vert1)
            e_weight[e0_1] = row[2]

    graph.vertex_properties["name"] = v_name
    graph.vertex_properties["color"] = v_color
    graph.edge_properties["weight"] = e_weight

    graph_draw(
        graph,
        vertex_text=graph.vertex_properties["name"],
        vertex_font_family="Arial",
        vertex_size=50,
        vertex_pen_width=2,
        vertex_fill_color=graph.vertex_properties["color"],
        edge_pen_width=graph.edge_properties["weight"],
        bg_color=[1, 1, 1, 1],
        output="friend_network_graph.png"
    )

if __name__ == "__main__":
    generate_graph("connections.csv")
