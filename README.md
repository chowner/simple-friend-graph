# Simple-Friend-Graph

Takes in a csv of friends and connection strength (e.g 1 (acquaintance) to 10 (best friend)) and dumps it into a colorful graph

## Dependencies
* python3
* graph-tool (https://graph-tool.skewed.de/)
